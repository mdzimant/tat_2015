Test Case : Success login to yandex mail
Steps :
1. open http://www.yandex.ru/
2. Click button "Войти"
3. Login input - type correct login name
4. Password input - type correct password
5. Click button "Войти"
6. Validate success login (mail account link shold be on the top-right of page)


Test Case : Unsuccess login to yandex mail : incorrect password
Steps :
1. open http://www.yandex.ru/
2. Click button "Войти"
3. Login input - type correct login name
4. Password input - type INcorrect password
5. Click button "Войти"
6. Validate UNsuccess login (message with text "Неправильная пара логин-пароль! Авторизоваться не удалось." should be on page)