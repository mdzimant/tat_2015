package com.epam.gomel.tat.lesson_5.pages;

import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 3/17/2015.
 */
public class MailboxBasePage extends AbstractBasePage {

    private static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    private static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");

    public MailInboxListPage openInboxPage() {
        browser.click(INBOX_LINK_LOCATOR);
        return new MailInboxListPage();
    }

    public MailSentListPage openSentPage() {
        browser.click(SENT_LINK_LOCATOR);
        return new MailSentListPage();
    }

}
